from django.contrib.gis.db import models
from django.conf import settings
import json


MANAGE_MODELS = False
CONNECTION_ALIAS = getattr(settings, "MDQ_CONNECTION_ALIAS", "default")


class BaseModel(models.Model):
    class Meta:
        abstract = True

    objects = models.Manager().db_manager(CONNECTION_ALIAS)


class DataSetDescription(BaseModel):
    """
    This model represents a data set
    """

    class Meta:
        managed = MANAGE_MODELS
        db_table = "data_set_description"

    # auto-incremented
    id = models.IntegerField(primary_key=True)

    # a 2 characters code for Commune, Quartier, Secteur Statistique
    level = models.CharField(max_length=2)

    # an integer for the year of the value
    year = models.IntegerField()

    # name for this dataset in french and dutch
    name_fr = models.TextField()
    name_nl = models.TextField()

    # description for this dataset in french and dutch
    description_fr = models.TextField()
    description_nl = models.TextField()

    # a dataset id that would overlay
    with_extra = models.IntegerField(blank=True, null=True)

    # unit in french
    unit_fr = models.CharField(max_length=32)

    # unit in dutch
    unit_nl = models.CharField(max_length=32)

    indicator_id = models.IntegerField(db_column="IndicatorId", blank=True, null=True)

    year_label = models.CharField(max_length=100, blank=True, null=True)

    decimals = models.IntegerField(blank=True, null=True)

    def display_name(self, lang):
        name = getattr(self, f"name_{lang}")
        unit = getattr(self, f"unit_{lang}")
        if unit is None or len(unit.strip()) == 0:
            return name
        return f"{name} ({unit})"


class DataSetSource(BaseModel):
    """
    This model represents a data set source
    """

    class Meta:
        managed = MANAGE_MODELS
        db_table = "data_set_source"

    # auto-incremented
    id = models.IntegerField(primary_key=True)

    # a reference to a dataset description
    description = models.ForeignKey(DataSetDescription, on_delete=models.CASCADE)

    # source URL in french
    source_url_fr = models.URLField()

    # source URL in dutch
    source_url_nl = models.URLField()

    # source URL label in french
    source_label_fr = models.TextField()

    # source URL lable in dutch
    source_label_nl = models.TextField()

    def get_label(self, lang):
        return getattr(self, f"source_label_{lang}")

    def get_url(self, lang):
        return getattr(self, f"source_url_{lang}")


class DataSetEntry(BaseModel):
    """
    This model represents a uniq value in a dataset
    """

    class Meta:
        managed = MANAGE_MODELS
        db_table = "data_set_entry"

    # auto-incremented
    id = models.IntegerField(primary_key=True)

    creation_date = models.DateTimeField()

    # a reference to the dataset containing this entry
    dataset_description_id = models.IntegerField()

    # an integer referencing a feature in the relevant layer
    # it might end up being a foreign key to an adjacent
    # geographic table
    feature_id = models.IntegerField()

    # The actual value as a floating point number
    value = models.FloatField()

    # flag HS or GM
    flag = models.CharField(max_length=2)


class LegendItem(BaseModel):
    """
    This model represents a legend item
    """

    class Meta:
        managed = MANAGE_MODELS
        db_table = "legend_item"

    # auto-incremented
    id = models.IntegerField(primary_key=True)

    # a reference to the dataset containing this item
    dataset_description_id = models.IntegerField()

    # the lower value (inclusive) of the interval for this legend item
    min_value = models.FloatField()

    # the upper value (exclusive) of the interval for this legend item
    max_value = models.FloatField()

    # the color for this legend item
    # https://developer.mozilla.org/en-US/docs/Web/CSS/color_value
    color = models.CharField(max_length=128)

    # a label in french and dutch  for this legend item
    label_fr = models.TextField()
    label_nl = models.TextField()

    # flag HS or GM
    flag = models.CharField(max_length=2)


class LegendExtra(BaseModel):
    """
    This model represents aan extra mention below legends
    """

    DATA_TYPE_CHOICES = (
        ("T", ("threshold?")),
        ("S", ("sum?")),
        ("A", ("average?")),
    )

    class Meta:
        managed = MANAGE_MODELS
        db_table = "legend_extra"

    # auto-incremented
    id = models.IntegerField(primary_key=True)

    # a reference to the dataset containing this item
    dataset_description_id = models.IntegerField()

    # a ordering index
    sort_order = models.IntegerField()

    # A languague code
    lang = models.CharField(max_length=2)

    # label
    label = models.CharField(max_length=256)

    # value
    value = models.CharField(max_length=256)

    # data type
    data_type = models.CharField(
        max_length=1, db_column="Type", choices=DATA_TYPE_CHOICES
    )


class GeoEntityBase(BaseModel):
    """
    ???
    """

    class Meta:
        managed = False
        db_table = "GeoEntityBase"

    id = models.IntegerField(primary_key=True, db_column="GE_ID")
    code = models.CharField(max_length=1, db_column="GE_Code")


class GeoEntityName(BaseModel):
    """
    ???
    """

    class Meta:
        managed = False
        db_table = "GeoEntity_lang"
        unique_together = ("entity_id", "lang")

    entity_id = models.ForeignKey(
        "mdq.GeoEntity",
        primary_key=True,
        unique=False,
        db_column="Id",
        on_delete=models.DO_NOTHING,
    )
    lang = models.CharField(max_length=2, db_column="Language_lg")
    name = models.CharField(max_length=300, db_column="Name")


class GeoEntity(BaseModel):
    """
    ???
    """

    class Meta:
        managed = False
        db_table = "GeoEntity"

    id = models.IntegerField(primary_key=True, db_column="Id")
    geo_entity_type = models.ForeignKey(
        GeoEntityBase, db_column="Geo_entity_type", on_delete=models.DO_NOTHING
    )
    code = models.CharField(max_length=300, db_column="EntityCode")
    geom = models.PolygonField(dim=2, srid=31370, db_column="Mpoly")


CACHED_ENTITIES_BY_ID = {}


def get_entities(level):
    global CACHED_ENTITIES_BY_ID
    if level not in CACHED_ENTITIES_BY_ID:
        CACHED_ENTITIES_BY_ID[level] = {}
        cache = CACHED_ENTITIES_BY_ID[level]
        entities = GeoEntity.objects.filter(
            geo_entity_type__code=level, geom__isnull=False
        )
        for entity in entities:
            names = dict([(n.lang, n.name) for n in entity.geoentityname_set.all()])
            geom = json.loads(entity.geom.geojson)
            cache[entity.id] = {
                "names": names,
                "code": entity.code,
                "geom": geom,
            }

    return CACHED_ENTITIES_BY_ID[level]
