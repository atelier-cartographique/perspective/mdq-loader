from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.utils.text import slugify
from mdq.models import DataSetDescription, DataSetEntry, get_entities
from .common import level_name, make_url


def serialize_geo_entry(entry, entities, lang):
    feature_id = entry.feature_id
    entity = entities.get(entry.feature_id)
    if entity is not None:
        names = entity["names"]
        geom = entity["geom"]
        if entry.flag == "":
            value = entry.value
        else:
            value = None
        return {
            "type": "Feature",
            "id": feature_id,
            "geometry": geom,
            "properties": {
                "id": entity["code"],
                "name": names[lang],
                "value": value,
            },
        }
    return None


def get_dataset_geojson(request, dataset_id, lang):
    description = get_object_or_404(DataSetDescription, pk=dataset_id)
    entities = get_entities(description.level)
    entries = DataSetEntry.objects.filter(dataset_description_id=description.id)

    req_get = request.GET
    if req_get is not None and "filter" in req_get:
        query_filters = req_get.getlist("filter")
        filter = list(map(int, query_filters))
        entries = entries.filter(feature_id__in=filter)

    features = []
    for e in entries:
        if e is not None:
            s = serialize_geo_entry(e, entities, lang)
            if s is not None:
                features.append(s)

    url = make_url(description, lang)
    response = JsonResponse(
        {
            "type": "FeatureCollection",
            "features": list(features),
            "crs": {"type": "name", "properties": {"name": "EPSG:31370"}},
            "metadata": {
                "title": getattr(description, "name_{}".format(lang)),
                "period": description.year_label,
                "scale": level_name(description.level, lang),
                "unit": getattr(description, "unit_{}".format(lang)),
                "url": url if url is not None else "",
            },
        }
    )

    response["Content-Disposition"] = 'attachment; filename="{}_{}_{}.geojson"'.format(
        slugify(getattr(description, "name_{}".format(lang))),
        description.year,
        slugify(level_name(description.level, lang)),
    )

    return response
