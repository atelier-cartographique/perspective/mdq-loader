from django.http.response import FileResponse
from pathlib import PosixPath
from tempfile import TemporaryDirectory
from xlsxwriter import Workbook
from functools import reduce
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.utils.text import slugify
from mdq.models import (
    DataSetDescription,
    DataSetEntry,
    DataSetSource,
    LegendExtra,
    get_entities,
)
from .common import base_url, level_name, make_url


LABELS = {
    "data": {
        "en": "Data",
        "fr": "Données",
        "nl": "Data",
    },
    "metadata": {
        "en": "Metadata",
        "fr": "Metadonnées",
        "nl": "Metadata",
    },
    "rbc": {
        "en": "RBC",
        "fr": "RBC",
        "nl": "BHG",
    },
    "average": {
        "en": "Average of territories",
        "fr": "Moyenne des territoires affichés",
        "nl": "Gemiddelde getoonde gebieden",
    },
    "nd": {
        "en": "* GM: not available",
        "fr": "* GM: non disponible",
        "nl": "* GM: niet beschikbaar",
    },
    "vs": {
        "en": "* HS: under floor value",
        "fr": "* HS: valeur soumise au seuil",
        "nl": "* HS: onder de drempelwaarde",
    },
    "min": {
        "en": "Minimum RBC",
        "fr": "Minimum RBC",
        "nl": "Minimum BHG",
    },
    "max": {
        "en": "Maximum RBC",
        "fr": "Maximum RBC",
        "nl": "Maximum BHG",
    },
    "level": {
        "en": "Level",
        "fr": "Échelle",
        "nl": "Schaal",
    },
    "unit": {
        "en": "Unit",
        "fr": "Unité",
        "nl": "Eenheid",
    },
    "sources": {
        "en": "Sources",
        "fr": "Sources",
        "nl": "Bronnen",
    },
    "ibsa": {
        "en": "IBSA",
        "fr": "IBSA",
        "nl": "BISA",
    },
    "na": {
        "fr": '"-": Valeur non disponible ou seuillée',
        "nl": '"-": waarde niet beschikbaar of onder de drempelwaarde',
    },
}


def label_lookup(lang):
    def inner(key):
        return LABELS[key][lang]

    return inner


def sort_key(row):
    try:
        return int(row[0])
    except:
        return row[0]


def get_dataset_table(request, dataset_id, lang):
    description = get_object_or_404(DataSetDescription, pk=dataset_id)
    entities = get_entities(description.level)
    entries = DataSetEntry.objects.filter(dataset_description_id=description.id)
    label = label_lookup(lang)
    req_get = request.GET
    if req_get is not None and "filter" in req_get:
        query_filters = req_get.getlist("filter")
        filter = list(map(int, query_filters))
        entries = entries.filter(feature_id__in=filter)

    full_title = f"{description.display_name(lang)} {description.year}"

    name = "{}_{}.xlsx".format(
        slugify(full_title), slugify(level_name(description.level, lang))
    )

    level = level_name(description.level, lang)
    decimal = description.decimals if description.decimals is not None else 2
    decimal_places = decimal * "0"

    extra_description = (
        get_object_or_404(DataSetDescription, pk=description.with_extra)
        if description.with_extra is not None
        else None
    )
    extra_data = (
        dict(
            [
                (e.feature_id, e.value)
                for e in DataSetEntry.objects.filter(
                    dataset_description_id=description.with_extra
                )
            ]
        )
        if description.with_extra is not None
        else None
    )

    def get_extra_data(entry: DataSetEntry):
        if extra_data is not None and entry.feature_id in extra_data:
            return extra_data[entry.feature_id]
        return None

    with TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        xls_path = root.joinpath(name)
        with Workbook(xls_path.as_posix()) as workbook:
            # sheet 0 (data)
            worksheet = workbook.add_worksheet(name=label("data"))
            worksheet.write(0, 2, description.display_name(lang))
            worksheet.set_column(2, 2, len(description.display_name(lang)))
            if extra_description is not None:
                worksheet.write(0, 3, extra_description.display_name(lang))
                worksheet.set_column(3, 3, len(extra_description.display_name(lang)))
            worksheet.write(1, 2, description.year_label)
            value_format = workbook.add_format({"num_format": f"#0.{decimal_places}"})

            index = 3
            worksheet.write_row(index, 0, ["code", level])
            index += 1
            more_info = LegendExtra.objects.filter(
                dataset_description_id=description.id, lang=lang
            ).order_by("sort_order")
            for info in more_info:
                index += 1
                worksheet.write(index, 1, info.label)
                worksheet.write(index, 2, info.value)

            rows = []
            meaningful_values = []
            max_entity_name_len = 0
            for entry in entries:
                entity = entities.get(entry.feature_id)
                if entity is not None:
                    if entry.flag == "":
                        value = entry.value
                        meaningful_values.append(value)
                    else:
                        value = "-"
                    entity_name = entity["names"].get(lang, "-?")
                    rows.append(
                        (entity["code"], entity_name, value, get_extra_data(entry))
                    )
                    max_entity_name_len = max(max_entity_name_len, len(entity_name))

            worksheet.set_column(1, 1, max_entity_name_len)
            rows.sort(key=sort_key)
            for index, (code, entity_name, main_value, extra_value) in enumerate(
                rows, index + 1
            ):

                worksheet.write_row(
                    index,
                    0,
                    [code, entity_name],
                )

                if isinstance(main_value, float):
                    worksheet.write_number(index, 2, main_value, value_format)
                else:
                    worksheet.write(index, 2, main_value)
                if extra_value is not None:
                    worksheet.write_number(index, 3, extra_value, value_format)

            # if len(meaningful_values) > 0:
            #     index += 2
            #     average = reduce(lambda acc, n: acc + n, meaningful_values) / len(
            #         meaningful_values
            #     )
            #     worksheet.write(index, 1, label("average"))
            #     worksheet.write_number(index, 2, average)
            # else:
            #     index += 2
            #     worksheet.write(index, 1, label("average"))
            #     worksheet.write(
            #         index, 2, f"Not enough meaningful values ({len(meaningful_values)})"
            #     )

            # worksheet.write(index + 1, 1, label("nd"))
            # worksheet.write(index + 2, 1, label("vs"))
            index += 2
            worksheet.write(index, 1, label("na"))

            url = make_url(description, lang)
            if url is not None:
                index += 2
                worksheet.write_row(index, 1, [label("metadata"), url])

            index += 2
            worksheet.write_row(index, 1, [label("ibsa"), base_url(lang)])

            # # sheet 1 (RBC)
            # worksheet = workbook.add_worksheet(name=label("rbc"))
            # worksheet.write(2, 1, description.display_name(lang))
            # worksheet.write(3, 1, description.year)
            # worksheet.write_row(4, 0, [label("min"), min(meaningful_values)])
            # worksheet.write_row(5, 0, [label("max"), max(meaningful_values)])
            # for index, info in enumerate(more_info, 6):
            #     worksheet.write(index, 0, info.label)
            #     worksheet.write(index, 1, info.value)

            # # sheet 2 (metadata)
            # worksheet = workbook.add_worksheet(name=label("metadata"))
            # worksheet.write(2, 1, description.display_name(lang))
            # worksheet.write(3, 1, description.year)
            # worksheet.write_row(4, 0, [label("level"), level])
            # worksheet.write_row(
            #     5, 0, [label("unit"), getattr(description, f"unit_{lang}", "")]
            # )
            # index = 7
            # worksheet.write(index, 0, label("sources"))
            # sources = DataSetSource.objects.filter(description_id=description.id)
            # for source in sources:
            #     index += 1
            #     worksheet.write_row(
            #         index, 0, [source.get_label(lang), source.get_url(lang)]
            #     )
            # worksheet.write(index + 3, 0, label("ibsa"))

        return FileResponse(xls_path.open("rb"), as_attachment=True, filename=name)
