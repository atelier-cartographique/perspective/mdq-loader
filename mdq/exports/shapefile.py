from django.http.response import FileResponse
from shapefile import Writer, POLYGON
from pathlib import PosixPath
from zipfile import ZipFile
from tempfile import TemporaryDirectory

from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.utils.text import slugify
from mdq.models import DataSetDescription, DataSetEntry, get_entities
from .common import level_name


def write_row(writer: Writer, entry, entities, lang):
    fid = entry.feature_id
    entity = entities.get(fid)
    if entity is not None:
        code = entity["code"]
        name = entity["names"][lang]
        geom = entity["geom"]
        if entry.flag == "":
            value = entry.value
        else:
            value = None
        print(f"write_row {code} {name} {value}")
        writer.record(code, name, value)
        writer.shape(geom)


# got that from QGIS export, hope it's correct
PRJ_STRING = """PROJCS["Belge_1972_Belgian_Lambert_72",GEOGCS["GCS_Belge 1972",DATUM["D_Belge_1972",SPHEROID["International_1924",6378388,297]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Lambert_Conformal_Conic"],PARAMETER["standard_parallel_1",51.16666723333333],PARAMETER["standard_parallel_2",49.8333339],PARAMETER["latitude_of_origin",90],PARAMETER["central_meridian",4.367486666666666],PARAMETER["false_easting",150000.013],PARAMETER["false_northing",5400088.438],UNIT["Meter",1]]"""


def get_dataset_shapefile(request, dataset_id, lang):
    description = get_object_or_404(DataSetDescription, pk=dataset_id)
    entities = get_entities(description.level)
    entries = DataSetEntry.objects.filter(dataset_description_id=description.id)
    req_get = request.GET
    if req_get is not None and "filter" in req_get:
        query_filters = req_get.getlist("filter")
        filter = list(map(int, query_filters))
        entries = entries.filter(feature_id__in=filter)

    name = "{}_{}_{}".format(
        slugify(getattr(description, "name_{}".format(lang))),
        slugify(description.year_label),
        slugify(level_name(description.level, lang)),
    )
    zip_name = name + ".zip"
    decimal = description.decimals if description.decimals is not None else 2
    with TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        shp_path = root.joinpath(name + ".shp")
        dbf_path = root.joinpath(name + ".dbf")
        shx_path = root.joinpath(name + ".shx")
        prj_path = root.joinpath(name + ".prj")
        with shp_path.open("wb") as shp, dbf_path.open("wb") as dbf, shx_path.open(
            "wb"
        ) as shx, Writer(shp=shp, dbf=dbf, shx=shx, shapeType=POLYGON) as writer:
            writer.field("ID", "C", size=255)
            writer.field("NAME", "C", size=255)
            writer.field("VALUE", "F", decimal=decimal)

            for entry in entries:
                write_row(writer, entry, entities, lang)

        with prj_path.open("w") as prj:
            prj.write(PRJ_STRING)

        zip_path = root.joinpath(zip_name)
        with ZipFile(zip_path.as_posix(), mode="x") as zip:
            zip.write(shp_path.as_posix(), shp_path.relative_to(root).as_posix())
            zip.write(dbf_path.as_posix(), dbf_path.relative_to(root).as_posix())
            zip.write(shx_path.as_posix(), shx_path.relative_to(root).as_posix())
            zip.write(prj_path.as_posix(), prj_path.relative_to(root).as_posix())

        return FileResponse(zip_path.open("rb"), as_attachment=True, filename=zip_name)
