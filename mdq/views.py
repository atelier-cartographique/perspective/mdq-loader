from io import BytesIO
from django.http import FileResponse, JsonResponse
from django.conf import settings
from django.db import connections
from django.shortcuts import get_object_or_404

from mdq.models import (
    DataSetDescription,
    DataSetEntry,
    DataSetSource,
    LegendExtra,
    LegendItem,
    GeoEntity,
)
from mdq.serializers import serialize_dataset, serialize_entity


def get_dataset(request, dataset_id):
    description = get_object_or_404(DataSetDescription, pk=dataset_id)
    entries = DataSetEntry.objects.filter(dataset_description_id=description.id)
    legend = LegendItem.objects.filter(dataset_description_id=description.id).order_by(
        "min_value"
    )
    extra = (
        LegendExtra.objects.filter(dataset_description_id=description.id)
        .exclude(data_type="T")
        .order_by("sort_order")
    )
    sources = DataSetSource.objects.filter(description_id=description.id)
    data = serialize_dataset(description, entries, legend, extra, sources)

    return JsonResponse(data)


REFERENCE_LAYERS_CACHE = {}


def get_reference_layer(_request, level):
    if level not in REFERENCE_LAYERS_CACHE:
        entities = GeoEntity.objects.filter(
            geo_entity_type__code=level, geom__isnull=False
        )
        REFERENCE_LAYERS_CACHE[level] = {
            "type": "FeatureCollection",
            "features": [serialize_entity(e) for e in entities],
        }
    return JsonResponse(REFERENCE_LAYERS_CACHE[level])


EXTRA_LAYERS = getattr(settings, "MDQ_EXTRA_LAYERS", [])


def get_extra_layers_list(_request):
    data = []
    for idx, extra in enumerate(EXTRA_LAYERS):
        data.append(
            {
                "id": idx,
                "name": extra["name"],
                "source": extra["source"],
                "geometry_type": extra["geometry_type"],
                "color": extra["color"],
            }
        )

    return JsonResponse(data, safe=False)


GEOJSON_QUERY = """
SELECT row_to_json(fc)
  FROM (
    SELECT
      'FeatureCollection' AS type,
      array_to_json(array_agg(f)) AS features
    FROM (
        SELECT
          'Feature' AS type,
          ST_AsGeoJSON(sd.geom, 2)::json AS geometry,
          gid as id,
          NULL as properties
        FROM  "{table}" AS sd
        {where_clause}
    ) AS f
  ) AS fc;
"""


def get_extra_layer(_request, id):
    extra = EXTRA_LAYERS[id]["data"]
    table = extra["table"]
    where = extra.get("where")
    where_clause = "" if where is None else "WHERE " + where
    query = GEOJSON_QUERY.format(table=table, where_clause=where_clause)
    con_alias = getattr(settings, "MDQ_CONNECTION_ALIAS")

    with connections[con_alias].cursor() as cursor:
        cursor.execute(query)
        row = cursor.fetchone()

    feature_collection = row[0]
    if feature_collection["features"] is None:  # can happen with a bbox
        feature_collection["features"] = []

    return JsonResponse(feature_collection)


class WMSBlank:
    _image = None

    def get_image(self):
        if self._image is None:
            from PIL import Image
            from io import BytesIO

            image = Image.new("RGB", (256, 256), "#fff")
            buffer = BytesIO()
            image.save(buffer, format="png")
            self._image = buffer.getvalue()

        return self._image


wmsBlank = WMSBlank()


def get_wms_blank(request):
    return FileResponse(
        BytesIO(wmsBlank.get_image()), as_attachment=True, filename="blank.png"
    )
