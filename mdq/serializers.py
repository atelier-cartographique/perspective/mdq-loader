import json


def serialize_description(description):
    return {
        "id": description.id,
        "level": description.level,
        "year": description.year_label,
        "name": {"fr": description.name_fr, "nl": description.name_nl},
        "description": {
            "fr": description.description_fr,
            "nl": description.description_nl,
        },
        "extra": description.with_extra,
        "unit": {
            "fr": description.unit_fr,
            "nl": description.unit_nl,
        },
    }


def serialize_entries(entries):
    return [
        {
            "id": entry.feature_id,
            "value": entry.value,
            "flag": entry.flag if entry.flag != "" else "NN",
        }
        for entry in entries
    ]


def serialize_legend(legend):
    return [
        {
            "low": item.min_value,
            "high": item.max_value,
            "color": item.color,
            "flag": item.flag if len(item.flag) else "NN",
            "label": {"fr": item.label_fr, "nl": item.label_nl},
        }
        for item in legend
    ]


def serialize_extra(extra):
    return [
        {"lang": e.lang, "label": e.label, "value": e.value, "tag": e.data_type}
        for e in extra
    ]


def serialize_sources(sources):
    return [
        {
            "id": source.id,
            "url": {
                "fr": source.source_url_fr,
                "nl": source.source_url_nl,
            },
            "label": {
                "fr": source.source_label_fr,
                "nl": source.source_label_nl,
            },
        }
        for source in sources
    ]


def serialize_dataset(description, entries, legend, extra, sources):
    data = serialize_description(description)
    data["entries"] = serialize_entries(entries)
    data["legend"] = serialize_legend(legend)
    data["sources"] = serialize_sources(sources)
    data["legendExtra"] = serialize_extra(extra)

    return data


def serialize_entity(entity):
    names = dict([(n.lang, n.name) for n in entity.geoentityname_set.all()])
    geom = json.loads(entity.geom.geojson)
    return {
        "type": "Feature",
        "id": entity.id,
        "geometry": geom,
        "properties": {"id": entity.id, "code": entity.code, "name": names},
    }


def serialize_geo_entry(entry, entities, lang):
    feature_id = entry.feature_id
    entity = entities.get(entry.feature_id)
    if entity is not None:
        names = entity["names"]
        geom = entity["geom"]
        return {
            "type": "Feature",
            "id": feature_id,
            "geometry": geom,
            "properties": {
                "id": feature_id,
                "name": names[lang],
                "value": entry.value,
                "flag": entry.flag,
            },
        }
    return None
